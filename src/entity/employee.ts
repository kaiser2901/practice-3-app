import { Department } from "./department";
import { FileImage } from "./fileImage";

export interface EmployeeRequest{
  employeeId : number,
  employeeName: string,
  employeePhone: string,
  employeeEmail: string,
  employeeCode: string,
  department: Department,
  fileImage : FileImage,

}

export interface EmployeeResponse{
  employeeId : number,
  employeeName: string,
  employeePhone: string,
  employeeEmail: string,
  employeeCode: string,
  department: Department,
  fileImage : FileImage,
  avatar : string | any;
}
