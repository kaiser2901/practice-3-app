import { EmployeeRequest } from "./employee";

export interface FileImage{
  fileId: string,
  fileName: string,
  fileType: string,
  fileData: Blob,
  employee: EmployeeRequest,
}
