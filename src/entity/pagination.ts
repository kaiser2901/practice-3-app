export interface PaginationAndSort{
  page : number;

  size : number;

  field : string;
}
