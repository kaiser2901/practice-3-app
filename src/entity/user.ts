import { Role } from "./role";

export interface UserResponse{
  userId : number;
  userName : string;
  userUsername : string;
  userPassword : string;
  userEmail: string;
  role : Role;
}

export interface UserRequest {
  userId : number;
  userName : string;
  userUsername : string;
  userPassword : string;
  userEmail: string;
  roleName : string[];
}
