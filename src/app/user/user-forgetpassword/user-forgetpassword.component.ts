import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-user-forgetpassword',
  templateUrl: './user-forgetpassword.component.html',
  styleUrls: ['./user-forgetpassword.component.css']
})
export class UserForgetpasswordComponent implements OnInit {
  forgotPassowrdForm : any;
  message!: any;
  constructor(private userService: UserService, private router : Router) { }

  ngOnInit(): void {
    this.initForm();
  }

  async forgotPassword(forgotPassowrdForm : any) {
    await this.userService.userForgotPassword(forgotPassowrdForm.value.userEmail).subscribe(
      (response) => {
        this.message = response;
        alert("We have sent a reset password link to your email. Please check.");
        this.router.navigate(['/user/login']);
      }, (error : HttpErrorResponse) => {
        alert("Could not find any user with this email " + forgotPassowrdForm.value.userEmail);
      }
    );
  }
  initForm() {
    this.forgotPassowrdForm = new FormGroup({
      userEmail: new FormControl('', [Validators.required, Validators.email])
    });
  }
  get userEmail() { return this.forgotPassowrdForm.get('userEmail'); }

}
