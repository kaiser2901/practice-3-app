import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { AbstractControl, FormControl, FormGroup, NgForm, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RoleService } from 'src/app/service/role.service';
import { TokenHandingService } from 'src/app/service/token-handing.service';
import { UserService } from 'src/app/service/user.service';
import { ValidatorService } from 'src/app/service/validator.service';
import { Role } from 'src/entity/role';
import { UserRequest } from 'src/entity/user';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css'],
  providers: [ValidatorService],
})
export class UserRegisterComponent implements OnInit {

  roles !: Role[];
  accessToken : any;
  registerValidationForm : any;
  userUsernameError: any;
  userEmailError: any;

  constructor(private userService : UserService,
    private validatorService: ValidatorService,
    private tokenHandler: TokenHandingService,
    private router: Router) { }

  ngOnInit(): void {
    this.accessToken = this.tokenHandler.getAccessToken();
    // this.getRoles();
    this.initForm();
    this.setValues();
  }

  // Validation
  initForm() {
    this.registerValidationForm = new FormGroup({
      userName : new FormControl('', [Validators.required, Validators.minLength(4)]),
      userUsername: new FormControl('', [Validators.required, Validators.minLength(4)]),
      userEmail: new FormControl('', [Validators.required, Validators.email]),
      userPassword: new FormControl('', [Validators.required, Validators.minLength(4)]),
      repassword: new FormControl('', [Validators.required, Validators.minLength(4)]),
    }, {validators: this.validatorService.passwordMatch('userPassword', 'repassword') });
  }

  get userName() { return this.registerValidationForm.get('userName'); }
  get userUsername() { return this.registerValidationForm.get('userUsername'); }
  get userEmail() { return this.registerValidationForm.get('userEmail'); }
  get userPassword() { return this.registerValidationForm.get('userPassword'); }
  get repassword() { return this.registerValidationForm.get('repassword'); }

  signUpUser(registerForm : any) {
    //check username
    this.validateFormRegister(registerForm);

  }
  async validateFormRegister(registerForm : any) {
    this.userUsernameError = await this.userService.checkUsernameExists(registerForm.value.userUsername).toPromise();
    this.userEmailError = await this.userService.checkEmailExists(registerForm.value.userEmail).toPromise();
    if(this.userUsernameError != null || this.userEmailError != null) {
      this.setSession(registerForm);
      location.reload();
    } else {
      this.removeSession();
      this.upsertUser(registerForm);
    }
  }
  setSession(registerForm:any) {
    sessionStorage.setItem('userNameRegister', registerForm.controls.userName.value);
    sessionStorage.setItem('userUsernameRegister', registerForm.controls.userUsername.value);
    sessionStorage.setItem('userEmailRegister', registerForm.controls.userEmail.value);
    sessionStorage.setItem('userUsernameExist', this.userUsernameError);
    sessionStorage.setItem('userEmailExist', this.userEmailError);
  }
  removeSession() {
    sessionStorage.removeItem('userNameRegister');
    sessionStorage.removeItem('userUsernameRegister');
    sessionStorage.removeItem('userEmailRegister');
    sessionStorage.removeItem('userUsernameExist');
    sessionStorage.removeItem('userEmailExist');
  }

  upsertUser(registerForm : any) {
    this.userService.upsert(registerForm.value).subscribe(
      (response : UserRequest) => {
        alert('Register Complete... ');
        this.router.navigate(['user/login']);
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  setValues() {
    if(sessionStorage.getItem('userNameRegister') && sessionStorage.getItem('userUsernameRegister') && sessionStorage.getItem('userEmailRegister')) {
      this.registerValidationForm.patchValue({
        userName : sessionStorage.getItem('userNameRegister'),
        userUsername: sessionStorage.getItem('userUsernameRegister'),
        userEmail: sessionStorage.getItem('userEmailRegister')
      }, (error: HttpErrorResponse) => {
        console.log(error.message);
      }
      );
    }
    if(sessionStorage.getItem('userUsernameExist')) {
      this.userUsernameError = sessionStorage.getItem('userUsernameExist');
    }
    if(sessionStorage.getItem('userEmailExist')) {
      this.userEmailError = sessionStorage.getItem('userEmailExist');
    }

  }
}
