import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, Injector, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { TokenHandingService } from 'src/app/service/token-handing.service';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  appComponent : any;
  headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
  errorLogin :any ;
  constructor(
    private userService: UserService,
    private injector: Injector,
    private router: Router,
    private tokenHandler: TokenHandingService) { }

  ngOnInit(): void {
    this.appComponent = this.injector.get(AppComponent, null);
  }

  checkLogin(formLogin : NgForm) : void {
    const params = new URLSearchParams();
    for (let key in formLogin.value) {
      params.set(key, formLogin.value[key]);
    }
    const body = params.toString();
    this.userService.generateToken(body).subscribe(
      (token : any) => {
        token = JSON.parse(token);
        sessionStorage.setItem("checkLoginUser", formLogin.value.username);
        sessionStorage.setItem("access_token","Bearer " + token.access_token);
        sessionStorage.setItem("refresh_token", token.refresh_token);
        this.appComponent?.checkLogin();
        this.router.navigate(['/']);
      },  (error : HttpErrorResponse) => {
        this.errorLogin = "Username or Password is invalid...";
      }
    );
  }


}
