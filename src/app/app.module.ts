import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DepartmentUpsertComponent } from './department/department-upsert/department-upsert.component';
import { DepartmentViewComponent } from './department/department-view/department-view.component';
import { EmployeeUpsertComponent } from './employee/employee-upsert/employee-upsert.component';
import { EmployeeViewComponent } from './employee/employee-view/employee-view.component';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { UserRegisterComponent } from './user/user-register/user-register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserForgetpasswordComponent } from './user/user-forgetpassword/user-forgetpassword.component';
import { UserResetpasswordComponent } from './user-resetpassword/user-resetpassword.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeViewComponent,
    EmployeeUpsertComponent,
    DepartmentViewComponent,
    DepartmentUpsertComponent,
    UserLoginComponent,
    UserRegisterComponent,
    UserForgetpasswordComponent,
    UserResetpasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
