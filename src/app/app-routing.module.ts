import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentUpsertComponent } from './department/department-upsert/department-upsert.component';
import { DepartmentViewComponent } from './department/department-view/department-view.component';
import { EmployeeUpsertComponent } from './employee/employee-upsert/employee-upsert.component';
import { EmployeeViewComponent } from './employee/employee-view/employee-view.component';
import { UserResetpasswordComponent } from './user-resetpassword/user-resetpassword.component';
import { UserForgetpasswordComponent } from './user/user-forgetpassword/user-forgetpassword.component';
import { UserLoginComponent } from './user/user-login/user-login.component';
import { UserRegisterComponent } from './user/user-register/user-register.component';

const routes: Routes = [
  {
    path: "employee/upsert",
    component: EmployeeUpsertComponent,
  },
  {
    path: "employee",
    component: EmployeeViewComponent
  },
  {
    path: "department/upsert",
    component: DepartmentUpsertComponent
  },
  {
    path: "department",
    component: DepartmentViewComponent
  },
  {
    path:"user/login",
    component: UserLoginComponent
  },
  {
    path:"user/register",
    component: UserRegisterComponent
  },
  {
    path:"user/forget-password",
    component: UserForgetpasswordComponent
  },
  {
    path:"user/reset-password",
    component: UserResetpasswordComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
