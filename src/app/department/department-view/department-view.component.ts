import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DepartmentService } from 'src/app/service/department.service';
import { TokenHandingService } from 'src/app/service/token-handing.service';
import { Department } from 'src/entity/department';

@Component({
  selector: 'app-department-view',
  templateUrl: './department-view.component.html',
  styleUrls: ['./department-view.component.css'],
})
export class DepartmentViewComponent implements OnInit {
  department!: Department;
  departments: Department[] = [];
  access_token : any;

  constructor(
    private departmentService: DepartmentService,
    private router: Router,
    private tokenHandler: TokenHandingService
  ) {}

  ngOnInit(): void {
    this.access_token = this.tokenHandler.getAccessToken();
    this.getDepartment();
  }

  //Get all department
  public getDepartment(): Department[] | any {
    this.departmentService.getAll(this.access_token).subscribe(
      (response) => {
        this.departments = response;
        return response;
      },
      (error: HttpErrorResponse) => {
        this.tokenHandler.checkLoginException(error);
      }
    );
  }

  //Delete department
  public deleteDepartment(departmentId : number){
    if(confirm("Do you want to delete this department? ") == true ){
      this.departmentService.deleteDepartment(departmentId, this.access_token).subscribe(
        (response) => {
          alert('complete');
          this.getDepartment();
        },
        (error : HttpErrorResponse)=> {
          this.tokenHandler.checkLoginException(error);
        }
      );
    }

  }

  //Search department

  //On Click Update
  updateDepartment(departmentId : number) {
    localStorage.setItem('departmentIdStore',departmentId.toString());
    this.router.navigate(['department/upsert']);
  }
}
