import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { DepartmentService } from 'src/app/service/department.service';
import { TokenHandingService } from 'src/app/service/token-handing.service';
import { Department } from 'src/entity/department';

@Component({
  selector: 'app-department-upsert',
  templateUrl: './department-upsert.component.html',
  styleUrls: ['./department-upsert.component.css']
})
export class DepartmentUpsertComponent implements OnInit {

  public department !: Department;
  stringDepartmentId !: string | null;
  departmentId !: number;
  access_token : any;

  constructor(
    private router : Router,
    private departmentService : DepartmentService,
    private tokenHandler: TokenHandingService,
    ) { }

  ngOnInit(): void {
    this.access_token = this.tokenHandler.getAccessToken();
    this.checkDepartmentId();
  }

  checkDepartmentId() : void{
    this.stringDepartmentId = localStorage.getItem('departmentIdStore');
    if(this.stringDepartmentId){
      this.departmentId = Number.parseInt(this.stringDepartmentId);
      this.findDepartmentById(this.departmentId);
      localStorage.removeItem('departmentIdStore');
    }
  }

  upsertDepartment(upsertDepartmentForm : NgForm) : void{
    if(this.departmentId){
      upsertDepartmentForm.value.departmentId = this.departmentId;
    }
    //Upsert Department
    this.departmentService.upsert(upsertDepartmentForm.value, this.access_token).subscribe(
      () => {
        alert('complete');
        upsertDepartmentForm.reset();
        this.router.navigate(['department']);
      },
      (error : HttpErrorResponse) => {
        this.tokenHandler.checkLoginException(error);
      }
    );
  }

  findDepartmentById(departmentId : number){
    this.departmentService.getById(departmentId, this.access_token).subscribe(
      (response) => {
        this.department = response;
      },
      (error : HttpErrorResponse) => {
        this.tokenHandler.checkLoginException(error);
      }
    );
  }

}
