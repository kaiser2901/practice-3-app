import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Practice3App';
  checkLoginUser: string | null = null;

  constructor(private router: Router){}
  ngOnInit(): void {
    this.checkLogin();
  }

  checkLogin(): void {
    if (sessionStorage.getItem('checkLoginUser')) {
      this.checkLoginUser = sessionStorage.getItem('checkLoginUser');
    } else {
      this.checkLoginUser = null;
    }
  }

  Logout() {
    this.checkLoginUser = null;
    sessionStorage.removeItem('checkLoginUser');
    sessionStorage.removeItem('access_token');
    sessionStorage.removeItem('refresh_token');
    this.router.navigate(['/user/login']);

  }
}
