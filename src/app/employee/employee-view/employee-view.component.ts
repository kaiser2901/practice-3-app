import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';
import { FileImageService } from 'src/app/service/file-image.service';
import { TokenHandingService } from 'src/app/service/token-handing.service';
import { EmployeeRequest, EmployeeResponse } from 'src/entity/employee';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.css'],
})
export class EmployeeViewComponent implements OnInit {
  employee!: EmployeeRequest;
  employees: EmployeeResponse[] | any = [];
  searchText!: string;
  employeesFilter: EmployeeResponse[] = [];
  employeesListSearch: EmployeeResponse[] | any = [];
  countPage: number = 1;
  currentSize!: number;
  currentPage!: number;
  field!: string;
  totalEmployee!: number;
  totalPages!: number;
  access_token: string = '';

  constructor(
    private employeeService: EmployeeService,
    private fileImageService: FileImageService,
    private router: Router,
    private tokenHandler: TokenHandingService
  ) {}

  ngOnInit(): void {
    this.checkToken();
    this.getEmployee();
    this.getEmployeesSearch();
  }

  checkToken(): void {
    if (sessionStorage.getItem('access_token')) {
      this.access_token = sessionStorage.getItem('access_token')!;
    }
  }

  //Get list employee
  getEmployee() {
    this.currentPage = this.currentPage === undefined ? 1 : this.currentPage;
    this.currentSize = this.currentSize === undefined ? 5 : this.currentPage;
    this.employeeService
      .getAllWithPagination(
        this.currentPage,
        this.currentSize,
        this.access_token
      )
      .subscribe(
        (response) => {
          this.employees = response.content;
          this.totalEmployee = response.totalElements;
          this.totalPages = response.totalPages;
          this.employeesFilter = this.employees;
          this.getAvatar(this.employeesFilter);
        },
        (error: HttpErrorResponse) => {
          this.tokenHandler.checkLoginException(error);
        }
      );
  }

  //Get list employee Search
  getEmployeesSearch() {
    this.employeeService.getAll(this.access_token).subscribe((response) => {
      this.employeesListSearch = response.response;
      this.getAvatar(this.employeesListSearch);
    });
  }

  //get Avatar
  getAvatar(list: any) {
    for (let emp of list) {
      if (emp.fileImage) {
        this.fileImageService
          .getUri(emp.fileImage?.fileId, this.access_token)
          .subscribe((response) => {
            emp.avatar = response;
          }, (error : HttpErrorResponse) => {
            this.tokenHandler.checkLoginException(error);
          });
      }
    }
  }

  //delete employee
  deleteEmployee(employeeId: number) {
    if (confirm('Do you want to delete this employee ?') == true) {
      this.employeeService.delete(employeeId, this.access_token).subscribe(
        (response) => {
          this.getEmployee();
        },
        (error: HttpErrorResponse) => {
          this.tokenHandler.checkLoginException(error);
        }
      );
    }
  }

  //Create session storage to binding employeeId.
  onUpdateEmployee(employeeId: number) {
    sessionStorage.setItem('employeeIdBinding', employeeId.toString());
    this.router.navigate(['employee/upsert']);
  }

  onCreateEmployee() {
    //remove sessionStorage
    sessionStorage.removeItem('employeeIdBinding');
    this.router.navigate(['employee/upsert']);
  }

  //Search employee
  searchEmployee(key: string) {
    const result: EmployeeRequest[] = [];
    this.employeesFilter = this.employeesListSearch.filter(
      (item: any) =>
        item.employeeName.toLowerCase().indexOf(key.toLowerCase()) !== -1 ||
        item.department.departmentName
          .toLowerCase()
          .indexOf(key.toLowerCase()) !== -1
    );
    if (!this.employeesFilter.length || !key) {
      this.employeesFilter = this.employees;
    }
  }

  //Count page
  counter(count: number) {
    let arrayCount: number[] = [];
    for (let i = 0; i < count; i++) {
      arrayCount = [i];
    }
    return new Array(count);
  }

  //Change page
  changePage(page: number) {
    this.currentPage = page + 1;
    this.getEmployee();
  }
}
