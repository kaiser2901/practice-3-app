import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DepartmentService } from 'src/app/service/department.service';
import { EmployeeService } from 'src/app/service/employee.service';
import { FileImageService } from 'src/app/service/file-image.service';
import { TokenHandingService } from 'src/app/service/token-handing.service';
import { Department } from 'src/entity/department';
import { EmployeeRequest } from 'src/entity/employee';

@Component({
  selector: 'app-employee-upsert',
  templateUrl: './employee-upsert.component.html',
  styleUrls: ['./employee-upsert.component.css'],
})
export class EmployeeUpsertComponent implements OnInit {
  employee!: EmployeeRequest;
  department!: Department;
  departments!: Department[];
  departmentIdSelected: number = 1;
  employeeId!: number;
  stringEmployeeId!: string | null;
  fileSelect!: File;
  imageShow: string = "https://thelifetank.com/wp-content/uploads/2018/08/avatar-default-icon.png";
  access_token : any = null;
  upsertFormEmployee : any;

  constructor(
    private employeeService: EmployeeService,
    private departmentService: DepartmentService,
    private fileImageService: FileImageService,
    private router: Router,
    private tokenHandler: TokenHandingService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.access_token = this.tokenHandler.getAccessToken();
    this.checkEmployee();
    this.getListDepartment();
  }
  initForm() {
    this.upsertFormEmployee = new FormGroup({
      employeeName: new FormControl('', [Validators.required, Validators.minLength(4)]),
      employeePhone: new FormControl('', [Validators.required, Validators.minLength(7), Validators.pattern("^[0-9]+$")]),
      employeeEmail: new FormControl('', [Validators.required, Validators.minLength(4), Validators.email]),
      departmentId: new FormControl(),
    })
  }
  get employeeName() { return this.upsertFormEmployee.get('employeeName'); }
  get employeePhone() { return this.upsertFormEmployee.get('employeePhone'); }
  get employeeEmail() { return this.upsertFormEmployee.get('employeeEmail'); }
  get departmentId() { return this.upsertFormEmployee.get('departmentId'); }

  //Check if employee Id exist
  checkEmployee() {
    //get employeeId from sessionStorage
    this.stringEmployeeId = sessionStorage.getItem('employeeIdBinding');
    if (this.stringEmployeeId) {
      //if exist employeeId
      this.employeeId = Number.parseInt(this.stringEmployeeId);
      this.findEmployeeById(this.employeeId);
    }
  }

  //get Employee By Id
  findEmployeeById(employeeId: number) {
    this.employeeService.getById(employeeId, this.access_token).subscribe(
      (response) => {
        this.employee = response;
        this.setValue();
        if (this.employee.department) {

          this.departmentIdSelected = this.employee.department.departmentId;
          this.getUriAvatar();
          console.log(this.upsertFormEmployee);

        }
      },
      (error: HttpErrorResponse) => {
        this.tokenHandler.checkLoginException(error);
      }
    );
  }

  setValue() {
    this.upsertFormEmployee.controls['employeeName'].setValue(this.employee.employeeName);
    this.upsertFormEmployee.controls.employeePhone.setValue(this.employee.employeePhone);
    this.upsertFormEmployee.controls.employeeEmail.setValue(this.employee.employeeEmail);
    this.upsertFormEmployee.controls.departmentId.setValue(this.employee.department.departmentId);
    // this.upsertFormEmployee.patchValue({
    //   employeeName: this.employee.employeeName,
    //   employeePhone: this.employee.employeePhone,
    //   employeeEmail: this.employee.employeeEmail,
    //   departmentId: this.employee.department.departmentId
    // });
  }

  getUriAvatar(){
    if(this.employee.fileImage?.fileId){
      this.fileImageService.getUri(this.employee.fileImage.fileId, this.access_token).subscribe(
        response => {
          this.imageShow = response;
        },(error : HttpErrorResponse)=> {
          this.tokenHandler.checkLoginException(error);
        }
      );
    }
  }

  upsertEmployee(upsertFormEmployee: any) {
    if (this.employeeId) {
      upsertFormEmployee.value.employeeId = this.employeeId;
    }
    upsertFormEmployee.value.departmentId = this.departmentIdSelected;
    this.employeeService.upsert(upsertFormEmployee.value, this.access_token).subscribe(
      (response: EmployeeRequest) => {
        this.uploadFile();
        alert('complete');
        upsertFormEmployee.reset();
      },
      (error: HttpErrorResponse) => {
        this.tokenHandler.checkLoginException(error);
      }
    );
  }

  // - data processing
  // Get Department
  getListDepartment() {
    this.departmentService.getAll(this.access_token).subscribe((response) => {
      this.departments = response;
    });
  }

  // Department on selected
  onSelectedDepartment(event: any) {
    this.departmentIdSelected = event.target.value;
  }

  //get selected file image
  selectedFile(e : any): void {
    if(e.target.files){
      this.fileSelect = e.target.files[0];
      // Show imgae
      var reader = new FileReader();
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = (event:any) => {
        this.imageShow = event.target.result;
      }
    }
  }

  // -- File

  //upload
  uploadFile() {
    const formData = new FormData();
    if(this.fileSelect) {
      formData.append('storeImage', this.fileSelect , this.fileSelect?.name);
      this.fileImageService.upload(formData, this.access_token).subscribe(
        (response) => {
          const fileId = response;
          this.mergeFileAndEmployee(this.employeeId, response);
        },
        (error: HttpErrorResponse) => {
          this.tokenHandler.checkLoginException(error);
        }
      );
    } else {
      this.router.navigate(['employee']);
    }
  }

  //merge file and employee
  mergeFileAndEmployee(employeeId: number, fileId: string) {
    this.employeeService.setFile(employeeId, fileId, this.access_token).subscribe(
      (response) => {
        this.router.navigate(['employee']);
      },
      (error: HttpErrorResponse) => {
        this.tokenHandler.checkLoginException(error);
      }
    );
  }
}
