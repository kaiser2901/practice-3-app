import { HttpErrorResponse } from '@angular/common/http';
import { Injectable, OnInit, Inject } from '@angular/core';
import { UserService } from './user.service';
import jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';

@Injectable({
  providedIn: 'root',
})
export class TokenHandingService {
  access_token: any;
  refresh_token: any;

  constructor(
    private router: Router,
    @Inject(UserService) private userService: UserService
  ) {}

  getAccessToken(): string | null {
    if (sessionStorage.getItem('access_token')) {
      this.access_token = sessionStorage.getItem('access_token')!;
      return this.access_token;
    }
    return null;
  }

  getDecodedToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  refreshToken() {
    try {
      if (sessionStorage.getItem('checkLoginUser')) {
        this.userService.refreshToken('Bearer ' + this.refresh_token).subscribe(
          (response: any) => {
            response = JSON.parse(response);
            sessionStorage.removeItem('access_token');
            sessionStorage.removeItem('refresh_token');
            sessionStorage.setItem(
              'access_token',
              'Bearer ' + response.access_token
            );
            sessionStorage.setItem('refresh_token', response.refresh_token);
            location.reload();
          },
          (error: HttpErrorResponse) => {
            this.removeSessionToken();
            alert('Something went wrong,  you need to login again......');
          }
        );
      }
    } catch (error) {
      this.removeSessionToken();
      alert('Something went wrong,  you need to login again......');
    }
  }

  checkLoginException(error: HttpErrorResponse) {
    try {
      if (error.status === 403) {
        alert('You are not authorized to access this link...');
        this.router.navigate(['/']);
      } else if (error.status === 401) {
        let sessionUsername = sessionStorage.getItem('checkLoginUser');
        if (sessionUsername) {
          this.refresh_token = sessionStorage.getItem('refresh_token');
          let username = this.getDecodedToken(this.refresh_token!);
          if (username.sub.toLowerCase() == sessionUsername.toLowerCase()) {
            this.refreshToken();
          } else {
            alert('Something went wrong,  you need to login again......');
            this.removeSessionToken();
          }
        } else {
          alert('Something went wrong,  you need to login again......');
          this.removeSessionToken();
        }
      } else {
        alert('Something went wrong...');
        this.removeSessionToken();
      }
    } catch (error2) {
      console.log(error2);
    }
  }
  removeSessionToken() {
    sessionStorage.removeItem('checkLoginUser');
    sessionStorage.removeItem('access_token');
    sessionStorage.removeItem('refresh_token');
    this.router.navigate(['user/login']);
    location.reload();
  }
}
