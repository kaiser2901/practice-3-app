import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FileImage } from 'src/entity/fileImage';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FileImageService {
  private apiServerUrl = environment.apiBaseUrl;
  constructor(private httpClient: HttpClient) {}

  // Upload File
  upload(formData: FormData, access_token: string): Observable<string> {
    return this.httpClient.post(`${this.apiServerUrl}/file/upload`, formData, {
      headers: new HttpHeaders().set('Authorization', access_token),
      responseType: 'text',
    });
  }

  // Get All File
  getAll(access_token: string): Observable<FileImage[]> {
    return this.httpClient.get<FileImage[]>(`${this.apiServerUrl}/file/all`, {headers: new HttpHeaders().set('Authorization', access_token)});
  }

  // Get Uri File - Link Download File
  getUri(fileId: string, access_token: string): Observable<any> {
    return this.httpClient.get(`${this.apiServerUrl}/file/get-uri/${fileId}`, {
      headers: new HttpHeaders().set('Authorization', access_token),
      responseType: 'text',
    });
  }

  // Delete file
  delete(id: string, access_token: string): Observable<any> {
    return this.httpClient.delete<any>(`${this.apiServerUrl}/delete/${id}`, {headers: new HttpHeaders().set('Authorization', access_token)});
  }
}
