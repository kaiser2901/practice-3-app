import { Injectable, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EmployeeResponse, EmployeeRequest } from 'src/entity/employee';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService{

  public access_token : string = "";
  headers = new HttpHeaders().set('Authorization', this.access_token);

  private apiServerUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient) {}



  checkToken(): void {
    if(sessionStorage.getItem("access_token")){
      this.access_token = sessionStorage.getItem("access_token")!;
    }
  }

  getAll(access_token:string) : Observable<any> {
    return this.http.get<any>(`${this.apiServerUrl}/employee/all`,{headers : new HttpHeaders().set('Authorization', access_token)});
  }

  getAllWithPagination(page : number, size : number, access_token:string) : Observable<any> {
    return this.http.get<any>(`${this.apiServerUrl}/employee/pagination?page=${page}&&size=${size}`, {headers : new HttpHeaders().set('Authorization', access_token)});
  }

  getAllWithPaginationAndSort(page : number, size : number , field : string) : Observable<EmployeeResponse[]> {
    field == null || field == undefined ? field = "employeeId" : field;
    return this.http.get<EmployeeResponse[]>(`${this.apiServerUrl}/employee/pagination-sort?page=${page}&&size=${size}&&field=${field}`);
  }

  getById(employeeId : number, access_token: string) : Observable<EmployeeRequest> {
    return this.http.get<EmployeeRequest>(`${this.apiServerUrl}/employee/find-by-id/${employeeId}`, {headers : new HttpHeaders().set('Authorization', access_token)});
  }

  upsert(employee : EmployeeRequest, access_token: string) : Observable<EmployeeRequest> {
    return this.http.post<EmployeeRequest>(`${this.apiServerUrl}/employee/upsert`, employee, {headers : new HttpHeaders().set('Authorization', access_token)});
  }

  delete(employeeId : number, access_token: string) : Observable<any> {
    return this.http.delete<any>(`${this.apiServerUrl}/employee/delete/${employeeId}`, {headers : new HttpHeaders().set('Authorization', access_token)});
  }

  setFile(employeeId: number, fileId: string, access_token: string) : Observable<EmployeeRequest> {
    return this.http.post<EmployeeRequest>(`${this.apiServerUrl}/employee/set-file`, {employeeId: employeeId, fileImageId: fileId}, {headers : new HttpHeaders().set('Authorization', access_token)});
  }

}
