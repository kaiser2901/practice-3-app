
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { UserRequest, UserResponse } from 'src/entity/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiServerUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient) { }

  generateToken(request: any){
    return this.http.post(`${this.apiServerUrl}/user/login`, request, { headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'}),responseType: 'text' as 'json'});
  }

  refreshToken(token : any){
    return this.http.post(`${this.apiServerUrl}/user/token/refresh`,token, { headers : new HttpHeaders().set('Authorization', token),responseType: 'text' as 'json'});
  }

  getAll(token : any) : Observable<any> {
    return this.http.get<any>(`${this.apiServerUrl}/user/all`, { headers : new HttpHeaders().set('Authorization', token)});
  }

  getAllWithPagination(page : number, size : number, token : any) : Observable<any> {
    return this.http.get<any>(`${this.apiServerUrl}/user/pagination?page=${page}&&size=${size}`, { headers : new HttpHeaders().set('Authorization', token)});
  }

  getAllWithPaginationAndSort(page : number, size : number , field : string, token : any) : Observable<UserResponse[]> {
    field == null || field == undefined ? field = "userId" : field;
    return this.http.get<UserResponse[]>(`${this.apiServerUrl}/user/pagination-sort?page=${page}&&size=${size}&&field=${field}`, { headers : new HttpHeaders().set('Authorization', token)});
  }

  getByName(userName : String, token : any) : Observable<UserRequest> {
    return this.http.get<UserRequest>(`${this.apiServerUrl}/user/find-by-name/${userName}`, { headers : new HttpHeaders().set('Authorization', token)});
  }

  upsert(user : UserRequest) : Observable<UserRequest> {
    return this.http.post<UserRequest>(`${this.apiServerUrl}/user/upsert`, user);
  }

  delete(userId : number, token : any) : Observable<any> {
    return this.http.delete<any>(`${this.apiServerUrl}/user/delete/${userId}`, { headers : new HttpHeaders().set('Authorization', token)});
  }

  checkUsernameExists(username:string) : Observable<string> {
    return this.http.get(`${this.apiServerUrl}/user/check-username/${username}`, {responseType: 'text'});
  }

  checkEmailExists(email:string) : Observable<string> {
    return this.http.get(`${this.apiServerUrl}/user/check-email/${email}`, {responseType: 'text'});
  }

  userForgotPassword(email:string) : Observable<string> {
    return this.http.post(`${this.apiServerUrl}/user/forgot-password?email=${email}`,email, {responseType: 'text'});
  }

  userResetPassword(token:string, password:string) : Observable<string> {
    return this.http.post(`${this.apiServerUrl}/user/reset-password`,{token,password}, {responseType: 'text'});
  }

}
