import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private apiServerUrl = environment.apiBaseUrl;
  constructor(private http: HttpClient) { }

  getAll(token : any) : Observable<any> {
    return this.http.get<any>(`${this.apiServerUrl}/role/all`, { headers : new HttpHeaders().set('Authorization', token)});
  }

  getAllWithPagination(page : number, size : number, token : any) : Observable<any> {
    return this.http.get<any>(`${this.apiServerUrl}/role/pagination?page=${page}&&size=${size}`, { headers : new HttpHeaders().set('Authorization', token)});
  }


}
