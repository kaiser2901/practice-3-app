import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Department } from 'src/entity/department';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DepartmentService {

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}

  //Get all department
  public getAll(access_token: string): Observable<Department[]> {
    return this.http.get<Department[]>(`${this.apiServerUrl}/department/all`, {headers : new HttpHeaders().set('Authorization', access_token)});
  }

  //get department by id
  public getById(departmentId: number, access_token: string): Observable<Department> {
    return this.http.get<Department>(
      `${this.apiServerUrl}/department/find-by-id/${departmentId}`, {headers : new HttpHeaders().set('Authorization', access_token)}
    );
  }

  //get department by code
  public getByCode(departmentCode: string, access_token: string): Observable<Department> {
    return this.http.get<Department>(
      `${this.apiServerUrl}/department/find-by-code/${departmentCode}`, {headers : new HttpHeaders().set('Authorization', access_token)}
    );
  }

  //upsert department
  public upsert(department: Department, access_token: string): Observable<Department> {
    return this.http.post<Department>(
      `${this.apiServerUrl}/department/upsert`,
      department , {headers : new HttpHeaders().set('Authorization', access_token)}
    );
  }

  //delete department
  public deleteDepartment(departmentId: number, access_token: string): Observable<any> {
    return this.http.delete<any>(
      `${this.apiServerUrl}/department/delete/${departmentId}`, {headers : new HttpHeaders().set('Authorization', access_token)}
    );
  }

}
