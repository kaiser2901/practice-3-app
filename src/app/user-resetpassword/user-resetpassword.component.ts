import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidatorService } from 'src/app/service/validator.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-user-resetpassword',
  templateUrl: './user-resetpassword.component.html',
  styleUrls: ['./user-resetpassword.component.css'],
})
export class UserResetpasswordComponent implements OnInit {

  token!: string;
  resetPasswordForm : any;

  constructor(private router: Router, private route: ActivatedRoute, private validatorService : ValidatorService, private userService: UserService) {}

  ngOnInit(): void {
    this.getTokenPassword();
    this.onInitForm();
  }

  getTokenPassword() {
    this.route.queryParams.subscribe((params) => {
      this.token = params['token'];
      console.log(this.token); // Print the parameter to the console.
    });
  }

  onInitForm() {
    this.resetPasswordForm = new FormGroup({
      userPassword: new FormControl('', [Validators.required, Validators.minLength(4)]),
      repassword: new FormControl('', [Validators.required, Validators.minLength(4)]),
    }, {validators: this.validatorService.passwordMatch('userPassword', 'repassword') });
  }

  get userPassword() { return this.resetPasswordForm.get('userPassword'); }
  get repassword() { return this.resetPasswordForm.get('repassword'); }

  resetPassword(resetPasswordForm : any) {
    this.userService.userResetPassword(this.token, resetPasswordForm.value.userPassword).subscribe(
      (response) => {
        alert(response);
        this.router.navigate(['/user/login']);
      }, (error : HttpErrorResponse) => {
        if(error.status === 400) {
          alert("The link has expired, please re-enter your email to receive a new link.");
        }
      }
    )
  }

}
